import {
  SubmitMessageBodyParams,
  SubmitMessageHeaderParams,
} from '../types/external';

export const SUBMIT_MESSAGE_EXAMPLE_1: {
  headerParams: SubmitMessageHeaderParams;
  bodyParams: SubmitMessageBodyParams;
} = {
  headerParams: {
    // C2
    fromParty: {
      id: 'oots-rel-ap1',
      role: 'http://sdg.europa.eu/edelivery/gateway',
      type: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
    },
    // C3
    toParty: {
      id: 'AP_R1_01',
      role: 'http://sdg.europa.eu/edelivery/gateway',
      type: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
    },
    collaborationInfo: {
      action: 'ExecuteQueryRequest',
      service: 'QueryManager',
      type: 'urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0',
      conversationId: '1234567890',
    },
    messageInfo: {
      // C1
      originalSender: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1',
      // C4
      finalRecipient: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4',
    },
  },
  bodyParams: {
    payload: {
      parts: [
        {
          href: 'message',
          contentType: 'text/xml',
          value:
            'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGhlbGxvPndvcmxkPC9oZWxsbz4=',
        },
        {
          href: 'message2',
          contentType: 'text/xml',
          value:
            'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGhlbGxvPndvcmxkPC9oZWxsbz4=',
        },
      ],
    },
  },
};

export const SUBMIT_MESSAGE_EXAMPLE_1_EXPECTED_PAYLOAD = [
  {
    attributes: { payloadId: 'cid:message' },
    value:
      'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGhlbGxvPndvcmxkPC9oZWxsbz4=',
  },
  {
    attributes: { payloadId: 'cid:message2' },
    value:
      'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGhlbGxvPndvcmxkPC9oZWxsbz4=',
  },
];

export const SUBMIT_MESSAGE_EXAMPLE_2: {
  headerParams: SubmitMessageHeaderParams;
  bodyParams: SubmitMessageBodyParams;
} = {
  headerParams: {
    // C3
    fromParty: {
      id: 'AP_R1_01',
      role: 'http://sdg.europa.eu/edelivery/gateway',
      type: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
    },
    // C2
    toParty: {
      id: 'oots-rel-ap1',
      role: 'http://sdg.europa.eu/edelivery/gateway',
      type: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
    },
    collaborationInfo: {
      action: 'ExecuteQueryRequest',
      service: 'QueryManager',
      type: 'urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0',
      conversationId: '1234567890',
    },
    messageInfo: {
      // C4
      originalSender: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4',
      // C1
      finalRecipient: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1',
    },
  },
  bodyParams: {
    payload: {
      parts: [
        {
          href: 'message',
          contentType: 'text/xml',
          value:
            'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGhlbGxvPndvcmxkPC9oZWxsbz4=',
        },
        {
          href: 'message2',
          contentType: 'text/xml',
          value:
            'PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGhlbGxvPndvcmxkPC9oZWxsbz4=',
        },
      ],
    },
  },
};
