/**
 * Based on:
 * https://ec.europa.eu/digital-building-blocks/wikis/display/TDD/4.5.1+-+Evidence+Request+Syntax+Mapping+-+Q4+2022#id-4.5.1EvidenceRequestSyntaxMappingQ42022-1.ExchangeDataModel:QueryRequest(EvidenceRequest)
 */
export interface OOTSQueryRequest {
  _declaration: {
    _attributes: {
      version: string;
      encoding: string;
    };
  };
  'query:QueryRequest': {
    _attributes: {
      'xmlns:xsi': string;
      'xmlns:rs': string;
      'xmlns:sdg': string;
      'xmlns:rim': string;
      'xmlns:query': string;
      'xmlns:xlink': string;
      'xmlns:xml': string;
      id: string;
      'xml:lang'?: string;
    };
    'rim:Slot': {
      _attributes: {
        name: string;
      };
      'rim:SlotValue': {
        _attributes: {
          'xsi:type': string;
          collectionType?: string;
        };
        'rim:Value'?: {
          'rim:LocalizedString'?: any;
          _text?: string;
        };
        'rim:Element'?: any;
        'sdg:Agent'?: any;
        'sdg:DataServiceEvidenceType'?: any;
      };
    }[];
    'query:ResponseOption': {
      _attributes: {
        returnType: string;
      };
    };
    'query:Query': {
      _attributes: {
        queryDefinition: string;
      };
      'rim:Slot': PersonSlot[] | any;
    };
  };
}

export interface payload {
  payload: payloadContent;
}

export interface payloadContent {
  _attributes: {
    payloadId: string;
    contentType: string;
  };
  value: {
    _text: string;
  };
}

export interface OOTSSoapBody {
  payload: payloadContent[];
}

export interface OOTSQueryResponse {
  'query:QueryResponse': {
    _attributes: {
      'xmlns:xsi': string;
      'xmlns:rs': string;
      'xmlns:sdg': string;
      'xmlns:rim': string;
      'xmlns:query': string;
      'xmlns:xlink': string;
      'xmlns:xml': string;
      requestId: string;
      status?: string;
    };
    'rim:Slot': {
      _attributes: {
        name: string;
      };
      'rim:SlotValue': {
        _attributes: {
          'xsi:type': string;
          collectionType?: string;
        };
        'rim:Value'?: {
          _text: string;
        };
        'rim:Element'?: any;
        'sdg:Agent'?: any;
      };
    }[];
    'rim:AviResponse'?: any;
    'rim:RegistryObjectList': {
      'rim:RegistryObject': RegistryObject;
    }[];
  };
}

export interface RegistryObject {
  'rim:Slot': {
    _attributes: {
      name: 'EvidenceMetadata';
    };
    'rim:SlotValue': {
      _attributes: {
        'xsi:type': string;
      };
      'sdg:Evidence': {
        'sdg:Identifier': {
          _text: string;
        };
        'sdg:IsAbout': {
          'sdg:NaturalPerson': {
            'sdg:Identifier': {
              _attributes: {
                schemeID: string;
              };
              _text: string;
            };
            'sdg:FamilyName': {
              _text: string;
            };
            'sdg:GivenName': {
              _text: string;
            };
            'sdg:DateOfBirth': {
              _text: string;
            };
          };
        };
        'sdg:IssuingAuthority': {
          'sdg:Identifier': {
            _attributes: {
              schemeID: string;
            };
            _text: string;
          };
          'sdg:Name': {
            _attributes: {
              lang: string;
            };
            _text: string;
          };
        };
        'sdg:IsConformantTo': {
          'sdg:EvidenceTypeClassification': {
            _text: string;
          };
          'sdg:Title': {
            _text: string;
          };
          'sdg:Description': {
            _text: string;
          };
        };
        'sdg:IssuingDate': {
          _text: string;
        };
        'sdg:Distribution': {
          'sdg:Format': {
            _text: string;
          };
        };
        'sdg:ValidityPeriod': {
          'sdg:StartDate': {
            _text: string;
          };
          'sdg:EndDate': {
            _text: string;
          };
        };
      };
    };
  };
  'rim:RepositoryItemRef': {
    _attributes: {
      'xlink:href': string;
      'xlink:title': string;
    };
  };
}

/**
 * Person slot
 */
export interface PersonSlot {
  _attributes: {
    name: 'NaturalPerson';
  };
  'rim:SlotValue': {
    _attributes: {
      'xsi:type': 'rim:AnyValueType';
    };
    'sdg:Person': {
      'sdg:LevelOfAssurance': {
        _text: string;
      };
      'sdg:Identifier'?: {
        attributes: {
          schemeID: string;
        };
        $value: string;
      };
      'sdg:FamilyName': {
        _text: string;
      };
      'sdg:GivenName': {
        _text: string;
      };
      'sdg:DateOfBirth': {
        _text: string;
      };
      'sdg:BirthName'?: {
        _text: string;
      };
      'sdg:PlaceOfBirth'?: {
        _text: string;
      };
      'sdg:CurrentAddress'?: Address;
      'sdg:SectorSpecificAttribute'?: SectorSpecificAttribute[];
    };
  };
}

interface SectorSpecificAttribute {
  'sdg:AttributeName': string;
  'sdg:AttributeURI': string;
  'sdg:AttributeValue': string;
}

interface Address {
  'sdg:FullAddress'?: string;
  'sdg:Thoroughfare'?: string;
  'sdg:LocatorDesignator'?: string;
  'sdg:AdminUnitLevel1'?: string;
  'sdg:AdminUnitLevel2'?: string;
  'sdg:PostCode'?: string;
  'sdg:PostCityName'?: string;
}

export interface OOTSQueryResponseError {
  _declaration: {
    _attributes: {
      version: string;
      encoding: string;
    };
  };
  'query:QueryResponse': {
    _attributes: {
      'xmlns:xsi': string;
      'xmlns:rs': string;
      'xmlns:sdg': string;
      'xmlns:rim': string;
      'xmlns:query': string;
      'xmlns:xlink': string;
      status: string;
      'xmlns:xml': string;
      requestId: string;
    };
    'rim:Slot': {
      _attributes: {
        name: string;
      };
      'rim:SlotValue': {
        _attributes: {
          'xsi:type': string;
          collectionType?: string;
        };
        'rim:Value'?: {
          _text: string;
        };
        'rim:Element'?: any;
        'sdg:Agent'?: any;
      };
    }[];
    'rs:Exception': {
      _attributes: {
        'xsi:type': 'rs:AuthorizationExceptionType';
        severity: 'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired';
        message: 'User redirection required';
      };
      'rim:Slot': {
        _attributes: {
          name: string;
        };
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': string;
            collectionType?: string;
          };
          'rim:Value'?: any;
          'rim:Element'?: any;
          'sdg:Agent'?: any;
        };
      }[];
    };
  };
}
