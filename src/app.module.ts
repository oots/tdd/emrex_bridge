import { Module } from '@nestjs/common';
import ServerModule from './modules/server/server.module';
import { ClientModule } from './modules/client/client.module';
import ConfigModule from './config/configuration';

@Module({
  imports: [ConfigModule, ClientModule, ServerModule],
  providers: [],
})
export class AppModule {}
export default AppModule;
