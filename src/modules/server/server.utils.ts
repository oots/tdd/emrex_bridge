import { DateTime } from 'luxon';
import { js2xml, json2xml, xml2json } from 'xml-js';
import {
  OOTSQueryResponseError,
  OOTSSoapBody,
  RegistryObject,
  payload,
} from '../../../lib/domibus';
import { UserInfo } from './datastore/datastore.interface';
import { randomUUID } from 'crypto';
import { OOTSQueryResponse } from '../../../lib/domibus';

export type OOTSQueryResponseErrorProps = {
  responseId: string;
  requestId: string;
  previewLocation: string;
};

type RegistryObjectProps = {
  attachmentId: string;
  user: UserInfo;
};

type OOTSQueryResponseProps = {
  user: UserInfo;
  requestId: string;
  elmo?: string;
};

export const createQueryResponseError = ({
  responseId,
  requestId,
  previewLocation,
}: OOTSQueryResponseErrorProps) => {
  const jsonResponse = {
    _declaration: {
      _attributes: {
        version: '1.0',
        encoding: 'utf-8',
      },
    },
    'query:QueryResponse': {
      _attributes: {
        'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
        'xmlns:rs': 'urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0',
        'xmlns:sdg': 'http://data.europa.eu/p4s',
        'xmlns:rim': 'urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0',
        'xmlns:query': 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0',
        'xmlns:xlink': 'http://www.w3.org/1999/xlink',
        status: 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Failure',
        'xmlns:xml': 'http://www.w3.org/XML/1998/namespace',
        requestId,
      },
      'rim:Slot': [
        {
          _attributes: {
            name: 'SpecificationIdentifier',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:StringValueType',
            },
            'rim:Value': {
              _text: 'oots-edm:v1.0',
            },
          },
        },
        {
          _attributes: {
            name: 'EvidenceResponseIdentifier',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:StringValueType',
            },
            'rim:Value': {
              _text: responseId,
            },
          },
        },

        {
          _attributes: {
            name: 'ErrorProvider',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:AnyValueType',
            },
            'sdg:Agent': {
              'sdg:Identifier': {
                _attributes: {
                  schemeID: 'urn:cef.eu:names:identifier:EAS:0204',
                },
                _text: 'DE7657587001',
              },
              'sdg:Name': {
                _attributes: {
                  lang: 'EN',
                },
                _text: 'Authority for school and occupational training',
              },
              'sdg:Address': {
                'sdg:FullAddress': 'Hamburger Str. 31',
                'sdg:LocatorDesignator': '31',
                'sdg:PostCode': '22083',
                'sdg:PostCityName': 'Hamburg',
                'sdg:AdminUnitLevel1': 'DE',
                'sdg:AdminUnitLevel2': 'DE60',
              },
              'sdg:Classification': {
                _text: 'ERRP',
              },
            },
          },
        },
        {
          _attributes: {
            name: 'EvidenceRequester',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:AnyValueType',
            },
            'sdg:Agent': {
              'sdg:Identifier': {
                _attributes: {
                  schemeID: 'urn:cef.eu:names:identifier:EAS:0190',
                },
                _text: 'NL22233223',
              },
              'sdg:Name': {
                _attributes: {
                  lang: 'EN',
                },
                _text: 'University of Amsterdam',
              },
            },
          },
        },
      ],
      'rs:Exception': {
        _attributes: {
          'xsi:type': 'rs:AuthorizationExceptionType',
          severity:
            'urn:sr.oots.tech.ec.europa.eu:codes:ErrorSeverity:EDMErrorResponse:PreviewRequired',
          message: 'User redirection required',
        },
        'rim:Slot': [
          {
            _attributes: {
              name: 'Timestamp',
            },
            'rim:SlotValue': {
              _attributes: {
                'xsi:type': 'rim:DateTimeValueType',
              },
              'rim:Value': { _text: DateTime.utc().toISO() },
            },
          },
          {
            _attributes: {
              name: 'PreviewLocation',
            },
            'rim:SlotValue': {
              _attributes: {
                'xsi:type': 'rim:StringValueType',
              },
              'rim:Value': {
                _text: previewLocation,
              },
            },
          },
          {
            _attributes: {
              name: 'PreviewDescription',
            },
            'rim:SlotValue': {
              _attributes: {
                'xsi:type': 'rim:InternationalStringValueType',
              },
              'rim:Value': {
                'rim:LocalizedString': [
                  {
                    _attributes: {
                      'xml:lang': 'NL',
                      value: 'NL preview',
                    },
                  },
                  {
                    _attributes: {
                      'xml:lang': 'RO',
                      value: 'RO preview',
                    },
                  },
                  {
                    _attributes: {
                      'xml:lang': 'EN',
                      value: 'EN preview',
                    },
                  },
                ],
              },
            },
          },
        ],
      },
    },
  } as OOTSQueryResponseError;
  const xmlResponse = js2xml(jsonResponse, { compact: true, spaces: 2 });
  return xmlResponse;
};

export const createRegistryObject = ({ user }: RegistryObjectProps) => {
  const { surname, dateOfBirth, forenames } = user.query;
  const person = {
    'sdg:NaturalPerson': {
      'sdg:Identifier': {
        _attributes: {
          schemeID: 'eidas',
        },
        _text: 'DK/DE/123456',
      },
      'sdg:FamilyName': {
        _text: surname,
      },
      ...(forenames && {
        'sdg:GivenName': {
          _text: forenames,
        },
      }),
      'sdg:DateOfBirth': {
        _text: dateOfBirth,
      },
    },
  };

  // TODO: Hard coded for now
  return {
    'rim:RegistryObject': {
      _attributes: {
        'xsi:type': 'rim:ExtrinsicObjectType',
        id: 'urn:uuid:' + randomUUID().toString(),
      },
      'rim:Slot': {
        _attributes: {
          name: 'EvidenceMetadata',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:AnyValueType',
          },
          'sdg:Evidence': {
            'sdg:Identifier': {
              _text: randomUUID().toString(),
            },
            'sdg:IsAbout': person,
            'sdg:IssuingAuthority': {
              'sdg:Identifier': {
                _attributes: {
                  schemeID: 'urn:cef.eu:names:identifier:EAS:9930',
                },
                _text: 'DE73524311',
              },
              'sdg:Name': {
                _attributes: {
                  lang: 'en',
                },
                _text: 'Civil Registration Office Berlin I',
              },
            },
            'sdg:IsConformantTo': {
              'sdg:EvidenceTypeClassification': {
                _text:
                  'https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/DK/' +
                  randomUUID().toString(),
              },
              'sdg:Title': {
                _attributes: {
                  lang: 'EN',
                },
                _text: 'Certificate of Birth',
              },
              'sdg:Description': {
                _attributes: {
                  lang: 'EN',
                },
                _text:
                  'An official certificate of birth of a person - with first name, surname, sex, date and place of birth, which is obtained from the birth register of the place of birth.',
              },
            },
            'sdg:IssuingDate': {
              _text: '1985-09-11',
            },
            'sdg:Distribution': {
              'sdg:Format': {
                _text: 'application/xml',
              },
            },
            'sdg:ValidityPeriod': {
              'sdg:StartDate': {
                _text: '2022-05-20',
              },
              'sdg:EndDate': {
                _text: '2023-05-20',
              },
            },
          },
        },
      },
      'rim:RepositoryItemRef': {
        _attributes: {
          'xlink:href': `cid:${randomUUID()}@example.oots.eu`,
          'xlink:title': 'Title',
        },
      },
    } as RegistryObject,
  };
};

export const createSoapBody = ({
  user,
  requestId,
  elmo,
}: OOTSQueryResponseProps): OOTSSoapBody => {
  const payloadArray: payload[] = [];

  const queryResponsePayload: payload = {
    payload: {
      _attributes: {
        payloadId: 'cid:' + randomUUID() + '@example.oots.eu',
        contentType: 'application/x-ebrs+xml',
      },
      value: {
        _text: Buffer.from(
          json2xml(
            JSON.stringify(
              createQueryResponse({
                user: user,
                requestId,
              }),
            ),
            {
              compact: true,
              spaces: 4,
            },
          ),
        ).toString('base64'),
      },
    },
  };

  const queryResponse: OOTSQueryResponse = JSON.parse(
    xml2json(
      Buffer.from(queryResponsePayload.payload.value._text, 'base64').toString(
        'utf-8',
      ),
      {
        compact: true,
        spaces: 4,
      },
    ),
  );

  let registryObjectList =
    queryResponse['query:QueryResponse']['rim:RegistryObjectList'];

  const UUIDsArray: string[] = [];

  if (!Array.isArray(registryObjectList)) {
    registryObjectList = [registryObjectList];
  }

  registryObjectList.forEach((r) => {
    const uuidString =
      r['rim:RegistryObject']['rim:RepositoryItemRef']._attributes[
        'xlink:href'
      ];

    UUIDsArray.push(uuidString);

    // const uuid = uuidString.substring(4).slice(0, -);
  });

  payloadArray.push(queryResponsePayload);

  if (elmo !== '') {
    const elmoPayload: payload = {
      payload: {
        _attributes: {
          payloadId: UUIDsArray[0],
          contentType: 'text/xml',
        },
        value: {
          _text: Buffer.from(elmo).toString('base64'),
        },
      },
    };

    payloadArray.push(elmoPayload);
  }

  return {
    payload: payloadArray.map((p) => {
      return p.payload;
    }),
  };
};

export const createQueryResponse = ({
  user,
  requestId,
}: OOTSQueryResponseProps): OOTSQueryResponse => {
  return {
    'query:QueryResponse': {
      _attributes: {
        'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
        'xmlns:rs': 'urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0',
        'xmlns:sdg': 'http://data.europa.eu/p4s',
        'xmlns:rim': 'urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0',
        'xmlns:query': 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0',
        'xmlns:xlink': 'http://www.w3.org/1999/xlink',
        status: 'urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success',
        'xmlns:xml': 'http://www.w3.org/XML/1998/namespace',
        requestId,
      },
      'rim:Slot': [
        {
          _attributes: {
            name: 'SpecificationIdentifier',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:StringValueType',
            },
            'rim:Value': {
              _text: 'oots-edm:v1.0',
            },
          },
        },
        {
          _attributes: {
            name: 'EvidenceResponseIdentifier',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:StringValueType',
            },
            'rim:Value': {
              _text: '5af62cce-debe-11ec-9d64-0242ac120002', //ID Of the request message
            },
          },
        },
        {
          _attributes: {
            name: 'IssueDateTime',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:DateTimeValueType',
            },
            'rim:Value': {
              _text: '2022-05-19T17:10:10.872Z',
            },
          },
        },
        {
          _attributes: {
            name: 'EvidenceProvider',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:CollectionValueType',
            },
            'rim:Element': {
              _attributes: {
                'xsi:type': 'rim:AnyValueType',
              },
              'sdg:Agent': {
                'sdg:Identifier': {
                  _attributes: {
                    schemeID: 'urn:cef.eu:names:identifier:EAS:0096',
                  },
                  _text: 'DK22233223',
                },
                'sdg:Name': {
                  _attributes: {
                    lang: 'en',
                  },
                  _text: 'Denmark University Portal',
                },
                'sdg:Address': {
                  'sdg:FullAddress': {
                    _text: 'Prince Street 15',
                  },
                  'sdg:LocatorDesignator': {
                    _text: '15',
                  },
                  'sdg:PostCode': {
                    _text: '1050',
                  },
                  'sdg:PostCityName': {
                    _text: 'Copenhagen',
                  },
                  'sdg:AdminUnitLevel1': {
                    _text: 'DK',
                  },
                  'sdg:AdminUnitLevel2': {
                    _text: 'DK011',
                  },
                },
                'sdg:Classification': {
                  _text: 'EP',
                },
              },
            },
          },
        },
        {
          _attributes: {
            name: 'EvidenceRequester',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:AnyValueType',
            },
            'sdg:Agent': {
              'sdg:Identifier': {
                _attributes: {
                  schemeID: 'urn:cef.eu:names:identifier:EAS:9930',
                },
                _text: 'DE73524311',
              },
              'sdg:Name': {
                _text: 'Civil Registration Office Berlin I',
              },
            },
          },
        },
        {
          _attributes: {
            name: 'ResponseAvailableDateTime',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:DateTimeValueType',
            },
            'rim:Value': {
              _text: '2022-05-30T15:00:00.000Z',
            },
          },
        },
      ],
      //   'rim:RegistryObjectList': elmo.map((data, index) =>
      //     createRegistryObject({ attachmentId: index.toString(), user }),
      //   ),
      'rim:RegistryObjectList': [
        createRegistryObject({ attachmentId: (0).toString(), user }),
      ],
    },
  } as OOTSQueryResponse;
};
