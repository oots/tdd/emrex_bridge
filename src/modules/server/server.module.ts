import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import ServerService from './server.service';
import ConfigModule from '../../config/configuration';
import DatastoreModule from '../server/datastore/datastore.module';
import { ServerController } from './server.controller';

@Module({
  imports: [ConfigModule, ScheduleModule.forRoot(), DatastoreModule],
  controllers: [ServerController],
  providers: [ServerService],
  exports: [ServerService],
})
export class ServerModule {}

export default ServerModule;
