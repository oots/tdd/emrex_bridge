export type  PreviewPayload = {
  previewId: string;
  userId: string;
  elmo?: string;
  approve?: boolean;
  error?: boolean;
};
