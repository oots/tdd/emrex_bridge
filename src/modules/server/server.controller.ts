import {
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  Post,
  Response,
} from '@nestjs/common';
import { PreviewPayload } from './server.interface';

import { ServerService } from './server.service';
import * as express from 'express';

@Controller()
export class ServerController {
  constructor(private serverService: ServerService) {}

  @Post('/store')
  async storeEmrexData(
    @Body() payload: any,
    @Response() res: express.Response,
  ) {
    const url = await this.serverService.storeEmrexData(payload);

    // Redirect to the preview page
    return res.redirect(303, url);
  }

  @Get('/preview/:previewId')
  @HttpCode(200)
  getPreviewInfo(@Param('previewId') previewId: string): Promise<any> {
    return this.serverService.getPreviewInfo(previewId);
  }

  @HttpCode(200)
  @Post('/preview')
  handlePreviewRequest(@Body() payload: PreviewPayload): Promise<any> {
    return this.serverService.handlePreviewRequest(payload);
  }

  @HttpCode(200)
  @Post('/returnUrl')
  handleAddReturnUrl(
    @Body() payload: { previewId: string; returnUrl: string },
  ): Promise<any> {
    return this.serverService.handleAddReturnUrl(payload);
  }
}
