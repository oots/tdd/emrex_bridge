import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Config } from 'src/config/configuration';
import { ConfigService } from '@nestjs/config';
import {
  listPendingMessages,
  OOTSSoapBody,
  PersonSlot,
  retrieveMessage,
  submitMessage,
} from '../../../lib/domibus';
import { isError } from '../../utils/results';
import { randomUUID } from 'crypto';
import { xml2json } from 'xml-js';
import BridgeDatastoreService from './datastore/datastore.service';
import { createQueryResponseError, createSoapBody } from './server.utils';
import { PreviewPayload } from './server.interface';
import { PREVIEW_STATUS } from './datastore/datastore.interface';
import { inflate } from 'pako';


@Injectable()
export class ServerService {
  private wsdlUrl: string;

  constructor(
    private configService: ConfigService<Config, true>,
    private datastore: BridgeDatastoreService,
  ) {
    this.wsdlUrl = `${configService.get<string>('SERVER_ACCESS_POINT')}?wsdl`;
  }

  @Cron(CronExpression.EVERY_5_SECONDS)
  async processMessages() {
    // Load pending messages
    const pendingMessagesResult = await listPendingMessages(this.wsdlUrl);

    // Check for errors
    if (isError(pendingMessagesResult)) {
      console.error(pendingMessagesResult.error);
      return;
    }

    const { messageIds } = pendingMessagesResult.data;

    console.log('Starting to process messages...');

    await Promise.all(
      messageIds.map(async (messageId) => {
        // Handle request
        await this.handleRequest(messageId);
      }),
    );

    console.log('Finished processing messages...');
  }

  async getPreviewInfo(previewId: string): Promise<any> {
    const previewInfo = this.datastore.getPreviewInfo(previewId);

    if (!previewInfo) {
      throw new NotFoundException('Preview info not found');
    }

    return previewInfo;
  }

  // Helper function for handling eDelivery requests
  async handleRequest(messageId: string) {
    console.log(
      `[${Date.now()}]: Starting to process message: ${messageId}...`,
    );

    // Retrieve message
    const messageResult = await retrieveMessage(this.wsdlUrl, messageId);

    console.log("Retrieved message's body...");
    // Check for errors
    if (isError(messageResult)) {
      console.error(messageResult.error);
      return;
    }

    const { header, body } = messageResult.data;

    const parts = body.payload as any[];

    const response = [];

    let previewLocationSet = false;

    const {
      Messaging: {
        UserMessage: {
          CollaborationInfo: {
            // Action: action,
            Service: {
              attributes: { type: serviceType },
              $value: serviceValue,
            },
            ConversationId: conversationId,
          },
          PartyInfo: {
            From: {
              PartyId: {
                attributes: { type: fromType },
                $value: fromId,
              },
              Role: fromRole,
            },
            To: {
              PartyId: {
                attributes: { type: toType },
                $value: toId,
              },
              Role: toRole,
            },
          },
        },
      },
    } = header;

    const originalSender =
      header.Messaging.UserMessage.MessageProperties.Property.find(
        (p: any) => p.attributes.name === 'originalSender',
      )?.$value;

    console.log('originalSender:' + originalSender);

    for (const part of parts) {
      // Decode base64 payload
      const stringPayload = Buffer.from(part.value, 'base64').toString('utf-8');
      const payload = JSON.parse(
        xml2json(stringPayload, {
          compact: true,
          spaces: 4,
        }),
      );
      const requestId = payload['query:QueryRequest']._attributes.id;

      // Check if previewLocation is present
      const previewLocation = payload['query:QueryRequest']['rim:Slot'].find(
        (slot: any) => slot._attributes.name === 'PreviewLocation',
      );

      if (previewLocation) {
        previewLocationSet = true;

        const previewLocationUrl =
          previewLocation['rim:SlotValue']['rim:Value']._text;

        const test = new URLSearchParams(previewLocationUrl.split('?')[1]);
        const previewId = test.get('sessionId');

        if (!previewId) {
          throw new InternalServerErrorException(
            'PreviewLocation URL is invalid',
          );
        }

        // PreviewLocation is present - this is the second request
        const previewInfo = this.datastore.getPreviewInfo(previewId as string);

        if (!previewInfo) {
          throw new NotFoundException('Preview info not found');
        }

        const { userInfo, userId } = previewInfo.data;

        const query =
          payload['query:QueryRequest']['query:Query']['rim:Slot'].find(
            (slot: any) => slot._attributes.name === 'NaturalPerson',
          );

        if (query._attributes.name === 'NaturalPerson') {
          const personSlot = query as PersonSlot;

          const {
            'rim:SlotValue': {
              'sdg:Person': {
                'sdg:GivenName': { _text: givenName },
                'sdg:FamilyName': { _text: familyName },
                'sdg:DateOfBirth': { _text: dateOfBirth },
              },
            },
          } = personSlot;

          if (userInfo.legalEntityCode !== 'NP') {
            throw new BadRequestException('Invalid query');
          }

          if (
            userInfo.query.surname !== familyName ||
            userInfo.query.dateOfBirth !== dateOfBirth ||
            (userInfo.query.forenames && userInfo.query.forenames !== givenName)
          ) {
            throw new BadRequestException('Invalid query');
          }
        } else {
          throw new BadRequestException('Invalid query');
        }

        // const cid = part.href.split(':')[1]; // TODO: Handle this better (we expect `cid:...` for now)
        const cid = part.attributes.payloadId.split(':')[1]; // TODO: Handle this better (we expect `cid:...` for now)

        this.datastore.updatePreviewInfo(previewId as string, {
          status: PREVIEW_STATUS.Initialized,
          domibusMessageInfo: {
            collaborationInfo: {
              action: 'ExecuteQueryResponse',
              type: serviceType,
              service: serviceValue,
              conversationId,
            },
            fromParty: {
              id: toId,
              type: toType,
              role: toRole,
            },
            toParty: {
              id: fromId,
              type: fromType,
              role: fromRole,
            },
            messageInfo: {
              finalRecipient: originalSender,
              originalSender: 'BR_R1_01',
            },
          },
          cid,
          requestId,
        });

        if (userId) {
          await this.handlePreviewRequest({
            previewId,
            userId,
          });
        }

        continue;
      }

      const previewId = randomUUID();
      const currentResponse = {
        payloadId: part.attributes.payloadId,
        responseId: randomUUID(),
        requestId: requestId,
        previewLocation: `${this.configService.get(
          'BRIDGE_FRONTEND',
        )}?sessionId=${previewId}`,
      } as {
        payloadId: string;
        responseId: string;
        requestId: string;
        previewLocation: string;
      };


      const query = payload['query:QueryRequest']['query:Query']['rim:Slot'].find(
        (slot: any) => slot._attributes.name === 'NaturalPerson',
      )

      if (!query) {
        throw new Error('Invalid query type');
      }

      const personSlot = query as PersonSlot;

      const {
        'rim:SlotValue': {
          'sdg:Person': {
            'sdg:GivenName': { _text: givenName },
            'sdg:FamilyName': { _text: familyName },
            'sdg:DateOfBirth': { _text: dateOfBirth },
          },
        },
      } = personSlot;

      console.log(
        `NaturalPerson: ${givenName} ${familyName} ${dateOfBirth.replace(
          /-/g,
          '',
        )}`,
      );

      // Save to datastore
      this.datastore.addPreviewInfo(previewId, {
        legalEntityCode: 'NP',
        query: {
          surname: familyName,
          dateOfBirth,
          forenames: givenName,
        },
      });

      response.push(currentResponse);
    }

    // Send resposne
    if (response.length > 0) {
      let actionPrev = 'ExecuteQueryResponse';
      if (!previewLocationSet) {
        actionPrev = 'ExceptionResponse';
      }

      const submitMessageResponse = await submitMessage(
        this.wsdlUrl,
        {
          collaborationInfo: {
            action: actionPrev,
            type: serviceType,
            service: serviceValue,
            conversationId: conversationId,
          },
          fromParty: {
            id: toId,
            type: toType,
            role: toRole,
          },
          toParty: {
            id: fromId,
            type: fromType,
            role: fromRole,
          },
          messageInfo: {
            finalRecipient: originalSender,
            originalSender: 'BR_R1_01',
          },
        },
        {
          payload: {
            parts: response.map((r) => {
              const clgRes = Buffer.from(
                createQueryResponseError({
                  responseId: r.responseId,
                  requestId: r.requestId,
                  previewLocation: r.previewLocation,
                }),
              ).toString('base64');

              const cid = r.payloadId.split(':')[1]; // TODO: Handle this better (we expect `cid:...` for now)
              return {
                href: cid,
                value: clgRes,
                contentType: 'application/x-ebrs+xml', // application/x-ebrs+xml
              };
            }),
          },
        },
      );

      if (isError(submitMessageResponse)) {
        console.error(submitMessageResponse.error);
        return;
      }

      console.log(
        `[${Date.now()}]: Submit message: ${
          submitMessageResponse.data.messageId
        }...`,
      );
    }

    console.log(
      `[${Date.now()}]: Finished processing message: ${messageId}...`,
    );
  }

  async handlePreviewRequest(payload: PreviewPayload) {
    let previewInfo = this.datastore.getPreviewInfo(payload.previewId);

    if (!previewInfo) {
      throw new NotFoundException(
        `Preview info for ${payload.previewId} not found`,
      );
    }

    const { userId, status } = previewInfo.data;

    if (!userId) {
      // We set the userId on the first request
      this.datastore.updatePreviewInfo(payload.previewId, {
        userId: payload.userId,
      });

      previewInfo = this.datastore.getPreviewInfo(payload.previewId);

      // If the status is pending, we wait for the second request
      if (status === PREVIEW_STATUS.Pending) {
        return;
      }
    }

    if (payload.userId !== previewInfo?.data.userId) {
      throw new BadRequestException(
        `User ${payload.userId} is not authorized to access preview ${payload.previewId}`,
      );
    }

    switch (status) {
      case PREVIEW_STATUS.Pending:
        throw new BadRequestException('Waiting to receive the second request');
      case PREVIEW_STATUS.Initialized:
        // Prepare and send Domibus message
        const { cid, userInfo, elmo, domibusMessageInfo, requestId } = previewInfo.data;

        if (!domibusMessageInfo) {
          throw new InternalServerErrorException(
            'Missing Domibus message info',
          );
        }

        if (!cid) {
          throw new InternalServerErrorException('Missing CID');
        }

        let messagePayload: OOTSSoapBody;

        if (payload.error === true) {
          const clgRes = Buffer.from(
            createQueryResponseError({
              responseId: payload.previewId,
              requestId,
              previewLocation: `${this.configService.get(
                'BRIDGE_FRONTEND',
              )}?sessionId=${payload.previewId}`,
            }),
          ).toString('base64');


          const submitMessageResponse = await submitMessage(
            this.wsdlUrl,
            { ...domibusMessageInfo },
            {
              payload: {
                parts: [
                  {
                    href: cid,
                    value: clgRes,
                    contentType: 'application/x-ebrs+xml',
                  },
                ],
              },
            },
          );

          if (isError(submitMessageResponse)) {
            console.error(submitMessageResponse.error);
            return;
          }

          console.log(
            `[${Date.now()}]: Submit message: ${
              submitMessageResponse.data.messageId
            }...`,
          );

          // Frontend should handle the redirect
          return { conversationId: domibusMessageInfo.collaborationInfo.conversationId };
        }

        if (payload.approve === true) {
          messagePayload = createSoapBody({
            user: userInfo,
            requestId,
            elmo: elmo ?? '',
          });
        } else {
          messagePayload = createSoapBody({
            user: userInfo,
            requestId,
            elmo: '',
          });
        }

        // Send response
        const submitMessageResponse = await submitMessage(
          this.wsdlUrl,
          { ...domibusMessageInfo },
          {
            payload: {
              parts: messagePayload.payload.map((m) => {
                const cid = m._attributes.payloadId.split(':')[1];
                return {
                  href: cid,
                  value: m.value._text,
                  contentType: m._attributes.contentType,
                };
              }),
            },
          },
        );

        if (isError(submitMessageResponse)) {
          console.error(submitMessageResponse.error);
          throw new InternalServerErrorException(
            'An error occurred when sending Domibus response',
          );
        }

        // We delete all stored data for this preview
        this.datastore.deletePreviewInfo(payload.previewId);

        console.log(
          `[${Date.now()}]: Submit message: ${
            submitMessageResponse.data.messageId
          }...`,
        );

        // Frontend should handle the redirect
        return { conversationId: domibusMessageInfo.collaborationInfo.conversationId };
      default:
        console.info(
          `Preview id ${payload.previewId} is in status ${previewInfo.data.status}`,
        );
        break;
    }
  }

  async storeEmrexData(payload: any): Promise<any> {
    const uuid = (payload.sessionId as string).replace(
      /(.{8})(.{4})(.{4})(.{4})(.{12})/,
      '$1-$2-$3-$4-$5',
    );
    const returnUrl = this.datastore.getReturnUrl(uuid);

    let url = `${this.configService.get(
      'BRIDGE_FRONTEND',
    )}?sessionId=${uuid}&returnurl=${returnUrl}`;

    if (payload.returnCode === 'NCP_CANCEL') {
      url += `&NCP_CANCEL=1`;
      return url;
    }

    if (payload.returnCode === 'NCP_NO_RESULTS') {
      url += `&NCP_NO_RESULTS=1`;
      return url;
    }

    if (payload.returnCode === 'NCP_ERROR') {
      url += `&NCP_ERROR=1`;
      return url;
    }

    const gzipedData = Buffer.from(payload.elmo, 'base64');

    const data = inflate(gzipedData, { to: 'string' }); // XML String

    this.datastore.updatePreviewInfo(uuid, { elmo: data });

    return url;
  }

  async handleAddReturnUrl(payload: {
    previewId: string;
    returnUrl: string;
  }): Promise<void> {
    const { previewId, returnUrl } = payload;

    this.datastore.setReturnUrl(previewId, returnUrl);

    return;
  }
}

export default ServerService;
