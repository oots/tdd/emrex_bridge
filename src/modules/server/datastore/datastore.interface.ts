export interface StoredObject<T> {
  created: number;
  data: T;
}

export enum PREVIEW_STATUS {
  Pending,
  Initialized,
  Finished,
  Error,
  Loading,
}

export type PreviewInfo = {
  status: PREVIEW_STATUS;
  error: string | null;
  // Auth information
  userId: string | null;
  // Query and Domibus information
  userInfo: UserInfo;
  domibusMessageInfo?: DomibusMessageInfo;
  elmo: string | null;
  cid?: string;
  returnUrl: string | null;
  requestId: string | null; 
};

type DomibusMessageInfo = {
  collaborationInfo: {
    action: string;
    type: string;
    service: string;
    conversationId: string;
  };
  fromParty: {
    id: string;
    type: string;
    role: string;
  };
  toParty: {
    id: string;
    type: string;
    role: string;
  };
  messageInfo: {
    finalRecipient: string;
    originalSender: string;
  };
};

export type UserInfo = {
  legalEntityCode: 'NP';
} & {
  legalEntityCode: 'NP';
  query: NPQuery;
};

export type NPQuery = {
  surname: string;
  dateOfBirth: string;
  forenames?: string;
};

export type PreviewInfoObject = StoredObject<PreviewInfo>;

export interface PreviewInfoStore {
  [previewId: string]: PreviewInfoObject;
}

export type ReturnUrlObject = StoredObject<{
  returnUrl: string;
}>;

export interface ReturnUrlStore {
  [previewId: string]: ReturnUrlObject;
}
