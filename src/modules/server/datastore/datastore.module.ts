import { Module } from '@nestjs/common';
import { BridgeDatastoreService } from './datastore.service';

@Module({
  imports: [],
  controllers: [],
  providers: [BridgeDatastoreService],
  exports: [BridgeDatastoreService],
})
export class BridgeDatastoreModule {}

export default BridgeDatastoreModule;
