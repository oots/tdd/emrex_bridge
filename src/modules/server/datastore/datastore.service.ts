import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import {
  PreviewInfoStore,
  PreviewInfoObject,
  UserInfo,
  PreviewInfo,
  PREVIEW_STATUS,
  ReturnUrlStore,
} from './datastore.interface';

export const MAX_SESSION_LIFETIME = 24 * 60 * 60 * 1000; // 24 hours

/**
 * Simple in-memory datastore for the bridge backend
 */
@Injectable()
export class BridgeDatastoreService {
  private readonly logger = new Logger(BridgeDatastoreService.name);

  private previewInfoStore: PreviewInfoStore;

  private returnUrlStore: ReturnUrlStore;

  constructor() {
    this.previewInfoStore = {};
    this.returnUrlStore = {};
  }

  addPreviewInfo(previewId: string, userInfo: UserInfo): void {
    this.previewInfoStore[previewId] = {
      created: Date.now(),
      data: {
        status: PREVIEW_STATUS.Pending,
        userInfo,
        error: null,
        userId: null,
        elmo: null,
        returnUrl: null,
        requestId: null,
      },
    };
  }

  updatePreviewInfo(previewId: string, data: Partial<PreviewInfo>): void {
    this.previewInfoStore[previewId].data = {
      ...this.previewInfoStore[previewId].data,
      ...data,
    };
  }

  getPreviewInfo(previewId: string): PreviewInfoObject | null {
    if (this.previewInfoStore[previewId]) {
      return structuredClone(this.previewInfoStore[previewId]);
    }

    return null;
  }

  deletePreviewInfo(previewId: string): void {
    delete this.previewInfoStore[previewId];
  }

  setReturnUrl(previewId: string, returnUrl: string): void {
    this.returnUrlStore[previewId] = {
      created: Date.now(),
      data: {
        returnUrl,
      },
    };
  }

  getReturnUrl(previewId: string): string | null {
    if (this.returnUrlStore[previewId]) {
      return this.returnUrlStore[previewId].data.returnUrl;
    }

    return null;
  }

  @Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT)
  clearStore(): void {
    this.previewInfoStore = {};
    this.returnUrlStore = {};
    this.logger.debug(
      'Preview Info Store and Return Url stores have been cleared',
    );
  }
}

export default BridgeDatastoreService;
