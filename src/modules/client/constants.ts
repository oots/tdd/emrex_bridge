import { randomUUID } from 'crypto';
import {
  PersonSlot,
  OOTSQueryRequest,
} from '../../../lib/domibus/types/external/oots';

export const requestData = {
  edelivery_request: {
    c1_party_id: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:C1',
    c4_party_id: 'urn:oasis:names:tc:ebcore:partyid-type:unregistered:C4',
    c2_party_id: 'oots-rel-ap1',
    c2_party_id_type:
      'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
    c2_role: 'http://sdg.europa.eu/edelivery/gateway',
    c3_party_id: 'AP_R1_01',
    c3_party_id_type:
      'urn:oasis:names:tc:ebcore:partyid-type:unregistered:oots-simulator',
    c3_role: 'http://sdg.europa.eu/edelivery/gateway',
  },
  action: 'ExecuteQueryRequest',
  service: 'QueryManager',
  type: 'urn:oasis:names:tc:ebcore:ebrs:ebms:binding:1.0',
};

export const payloadJSON: OOTSQueryRequest = {
  _declaration: {
    _attributes: {
      version: '1.0',
      encoding: 'utf-8',
    },
  },
  'query:QueryRequest': {
    _attributes: {
      'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
      'xmlns:rs': 'urn:oasis:names:tc:ebxml-regrep:xsd:rs:4.0',
      'xmlns:sdg': 'http://data.europa.eu/p4s',
      'xmlns:rim': 'urn:oasis:names:tc:ebxml-regrep:xsd:rim:4.0',
      'xmlns:query': 'urn:oasis:names:tc:ebxml-regrep:xsd:query:4.0',
      'xmlns:xlink': 'http://www.w3.org/1999/xlink',
      'xmlns:xml': 'http://www.w3.org/XML/1998/namespace',
      id: 'urn:uuid:' + randomUUID(),
      'xml:lang': 'en',
    },
    'rim:Slot': [
      {
        _attributes: {
          name: 'SpecificationIdentifier',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:StringValueType',
          },
          'rim:Value': {
            _text: 'oots-edm:v1.0',
          },
        },
      },
      {
        _attributes: {
          name: 'IssueDateTime',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:DateTimeValueType',
          },
          'rim:Value': {
            _text: '2021-02-14T19:20:30+01:00',
          },
        },
      },
      {
        _attributes: {
          name: 'Procedure',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:InternationalStringValueType',
          },
          'rim:Value': {
            'rim:LocalizedString': {
              _attributes: {
                'xml:lang': 'EN',
                value: 'V2',
              },
            },
          },
        },
      },
      {
        _attributes: {
          name: 'PossibilityForPreview',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:BooleanValueType',
          },
          'rim:Value': {
            _text: 'true',
          },
        },
      },
      {
        _attributes: {
          name: 'ExplicitRequestGiven',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:BooleanValueType',
          },
          'rim:Value': {
            _text: 'true',
          },
        },
      },
      {
        _attributes: {
          name: 'Requirements',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:CollectionValueType',
            collectionType:
              'urn:oasis:names:tc:ebxml-regrep:CollectionType:Set',
          },
          'rim:Element': {
            _attributes: {
              'xsi:type': 'rim:AnyValueType',
            },
            'sdg:Requirement': {
              'sdg:Identifier': {
                _text:
                  'https://sr.oots.tech.ec.europa.eu/requirements/cfcd4e22-30f1-48fa-93e4-6cf98cedbf40',
              },
              'sdg:Name': {
                _attributes: {
                  lang: 'EN',
                },
                _text: 'Proof of vehicle registration',
              },
            },
          },
        },
      },
      {
        _attributes: {
          name: 'EvidenceRequester',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:CollectionValueType',
          },
          'rim:Element': {
            _attributes: {
              'xsi:type': 'rim:AnyValueType',
            },
            'sdg:Agent': {
              'sdg:Identifier': {
                _attributes: {
                  schemeID: 'urn:cef.eu:names:identifier:EAS:0096',
                },
                _text: 'DK22233223',
              },
              'sdg:Name': {
                _attributes: {
                  lang: 'EN',
                },
                _text: 'Denmark University Portal',
              },
              'sdg:Address': {
                'sdg:FullAddress': {
                  _text: 'Prince Street 15',
                },
                'sdg:LocatorDesignator': {
                  _text: '15',
                },
                'sdg:PostCode': {
                  _text: '1050',
                },
                'sdg:PostCityName': {
                  _text: 'Copenhagen',
                },
                'sdg:AdminUnitLevel1': {
                  _text: 'DK',
                },
                'sdg:AdminUnitLevel2': {
                  _text: 'DK011',
                },
              },
              'sdg:Classification': {
                _text: 'ER',
              },
            },
          },
        },
      },
      {
        _attributes: {
          name: 'EvidenceProvider',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:AnyValueType',
          },
          'sdg:Agent': {
            'sdg:Identifier': {
              _attributes: {
                schemeID: 'urn:cef.eu:names:identifier:EAS:9930',
              },
              _text: 'DE73524311',
            },
            'sdg:Name': {
              _text: 'Civil Registration Office Berlin I',
            },
          },
        },
      },
    ],
    'query:ResponseOption': {
      _attributes: {
        returnType: 'LeafClassWithRepositoryItem',
      },
    },
    'query:Query': {
      _attributes: {
        queryDefinition: 'DocumentQuery',
      },
      'rim:Slot': [
        {
          _attributes: {
            name: 'EvidenceRequest',
          },
          'rim:SlotValue': {
            _attributes: {
              'xsi:type': 'rim:AnyValueType',
            },
            'sdg:DataServiceEvidenceType': {
              'sdg:Identifier': {
                _text: 'aa55b273-0698-4a96-91cc-4d980055c0e1',
              },
              'sdg:EvidenceTypeClassification': {
                _text:
                  'https://sr.oots.tech.ec.europa.eu/evidencetypeclassifications/DK/4a51a392-79a8-437b-b53c-8cddbf7210b5',
              },
              'sdg:Title': {
                _attributes: {
                  lang: 'EN',
                },
                _text: 'Vehicle registration evidence',
              },
              'sdg:Description': {
                _attributes: {
                  lang: 'EN',
                },
                _text: 'Proof of vehicle registration.',
              },
              'sdg:DistributedAs': {
                'sdg:Format': {
                  _text: 'application/xml',
                },
                'sdg:ConformsTo': {
                  _text:
                    'https://sr.oots.tech.ec.europa.eu/distributions/secondary-education-evidence-1.0.0',
                },
                'sdg:Transformation': {
                  _text:
                    'https://sr.oots.tech.ec.europa.eu/distributions/secondary-education-evidence-1.0.0/passed',
                },
              },
            },
          },
        },
      ],
    },
  },
};
