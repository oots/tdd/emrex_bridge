import { Module } from '@nestjs/common';
import { ClientDatastoreService } from './datastore.service';

@Module({
  imports: [],
  controllers: [],
  providers: [ClientDatastoreService],
  exports: [ClientDatastoreService],
})
export class ClientDatastoreModule {}

export default ClientDatastoreModule;
