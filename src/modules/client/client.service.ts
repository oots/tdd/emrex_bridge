import { HttpException, Injectable } from '@nestjs/common';
import {
  listPendingMessages,
  retrieveMessage,
  submitMessage,
} from '../../../lib/domibus/ws';
import {
  SubmitMessageHeaderParams,
  SubmitMessageBodyParams,
  ListPendingMessagesResponse,
  requestPayload,
  PersonSlot,
} from '../../../lib/domibus';
import { isError } from './../../utils/results';
import { Config } from '../../config/configuration';
import { ConfigService } from '@nestjs/config';
import { payloadJSON, requestData } from './constants';
import { js2xml } from 'xml-js';

import { Cron, CronExpression } from '@nestjs/schedule';
import ClientDatastoreService from './datastore/datastore.service';
import { MessageLog } from './datastore/datastore.interface';
import { randomUUID } from 'crypto';

@Injectable()
export class ClientService {
  private wsdlUrl: string;

  constructor(
    private configService: ConfigService<Config, true>,
    private datastore: ClientDatastoreService,
  ) {
    this.wsdlUrl = `${configService.get<string>('CLIENT_ACCESS_POINT')}?wsdl`;
  }

  async getMessageById(id: string): Promise<MessageLog | undefined> {
    const result = this.datastore.getMessageLogs(id);
    let finalResult = undefined;

    if (result) {
      finalResult = result.data;
    }

    return finalResult;
  }

  async getPendingMessages(): Promise<ListPendingMessagesResponse> {
    const result = await listPendingMessages(this.wsdlUrl);
    const isErr = isError(result);
    if (isErr) {
      throw new HttpException(result.error.toString(), 500);
    }
    return result.data;
  }

  async submitMessage(payload: requestPayload): Promise<{messageId: string, conversationId: string}> {
    const conversationId = payload.data.conversationId != undefined ? payload.data.conversationId: randomUUID();
    const headerParams: SubmitMessageHeaderParams = {
      fromParty: {
        id: requestData.edelivery_request.c2_party_id,
        type: requestData.edelivery_request.c2_party_id_type,
        role: requestData.edelivery_request.c2_role,
      },
      toParty: {
        id: requestData.edelivery_request.c3_party_id,
        type: requestData.edelivery_request.c3_party_id_type,
        role: requestData.edelivery_request.c3_role,
      },
      collaborationInfo: {
        action: requestData.action,
        service: requestData.service,
        type: requestData.type,
        conversationId,
      },
      messageInfo: {
        originalSender: requestData.edelivery_request.c1_party_id,
        finalRecipient: requestData.edelivery_request.c4_party_id,
      },
    };

    const person: PersonSlot = {
      _attributes: {
        name: 'NaturalPerson',
      },
      'rim:SlotValue': {
        _attributes: {
          'xsi:type': 'rim:AnyValueType',
        },
        'sdg:Person': {
          'sdg:LevelOfAssurance': {
            _text: 'High',
          },
          'sdg:FamilyName': {
            _text: payload.data.surName,
          },
          'sdg:GivenName': {
            _text: payload.data.name,
          },
          'sdg:DateOfBirth': {
            _text: payload.data.dateOfBirth,
          },
        },
      },
    };

    let payloadJSONReq = JSON.parse(JSON.stringify(payloadJSON));

    if (payload.data.previewLocation && payload.data.previewLocation !== '') {
      payloadJSONReq['query:QueryRequest']['rim:Slot'].push({
        _attributes: {
          name: 'PreviewLocation',
        },
        'rim:SlotValue': {
          _attributes: {
            'xsi:type': 'rim:StringValueType',
          },
          'rim:Value': {
            _text: payload.data.previewLocation,
          },
        },
      });
    } else {
      // Reset payloadJSONReq variable to original
      payloadJSONReq = JSON.parse(JSON.stringify(payloadJSON));
    }

    payloadJSONReq['query:QueryRequest']['query:Query']['rim:Slot'].push(
      person,
    );

    const payloadXML = js2xml(payloadJSONReq, {
      compact: true,
      spaces: 4,
    });

    const bodyParams: SubmitMessageBodyParams = {
      payload: {
        parts: [
          {
            href: randomUUID() + '@c2.example.com',
            contentType: 'application/x-ebrs+xml',
            value: Buffer.from(payloadXML).toString('base64'),
          },
        ],
      },
    };

    const result = await submitMessage(this.wsdlUrl, headerParams, bodyParams);
    const isErr = isError(result);

    if (isErr) {
      throw new HttpException(result.error.toString(), 500);
    }

    this.datastore.addMessageLog(conversationId);

    return {...result.data, conversationId};
  }

  @Cron(CronExpression.EVERY_5_SECONDS)
  async processMessages() {
    // Load pending messages
    const pendingMessagesResult = await listPendingMessages(this.wsdlUrl);

    // Check for errors
    if (isError(pendingMessagesResult)) {
      console.error(pendingMessagesResult.error);
      return;
    }

    const { messageIds } = pendingMessagesResult.data;

    console.log('Starting to process messages from client...');

    await Promise.all(
      messageIds.map(async (messageId) => {
        // Handle request
        await this.handleRequest(messageId);
      }),
    );

    console.log('Finished processing messages from client...');
  }

  async handleRequest(messageId: string) {
    console.log(
      `[${Date.now()}]: Starting to process message: ${messageId}... from client`,
    );

    // Retrieve message
    const messageResult = await retrieveMessage(this.wsdlUrl, messageId);

    // Check for errors
    if (isError(messageResult)) {
      console.error(messageResult.error);
      return;
    }

    const { header, body } = messageResult.data;

    const data = {
      header: JSON.stringify(header),
      body: JSON.stringify(body),
    };

    this.datastore.updateMessageLog(
      header.Messaging.UserMessage.CollaborationInfo.ConversationId,
      data,
    );
  }
}

export default ClientService;
