declare global {
  namespace NodeJS {
    interface ProcessEnv {
      SERVER_ACCESS_POINT: string;
      CLIENT_ACCESS_POINT: string;
    }
  }
}
