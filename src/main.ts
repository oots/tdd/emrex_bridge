import { NestFactory } from '@nestjs/core';
import type { NestExpressApplication } from '@nestjs/platform-express';
import * as bodyParser from 'body-parser';
import { AppModule } from './app.module';
import AllExceptionsFilter from './filters/all-exceptions.filter';
import * as fs from 'fs';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    httpsOptions: {
      key: fs.readFileSync('./secrets/example.key'),
      cert: fs.readFileSync('./secrets/example.crt'),
    },
  });
  app.enableCors();

  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

  // Set up the global exception filter
  app.useGlobalFilters(new AllExceptionsFilter());

  await app.listen(3001, '0.0.0.0');
}

bootstrap().catch((e) => {
  console.error(e);
});
