import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Response } from 'express';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    let error;
    let httpStatus;

    if (exception instanceof HttpException) {
      const exceptionResponse = exception.getResponse();
      error = new Error(
        typeof exceptionResponse === 'string'
          ? exceptionResponse
          : JSON.stringify(exceptionResponse),
      );

      httpStatus = exception.getStatus();
    } else {
      error = new Error(
        'The server encountered an internal error and was unable to complete your request.',
      );
      httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    }

    return response.status(httpStatus).json(error.message);
  }
}

export default AllExceptionsFilter;
