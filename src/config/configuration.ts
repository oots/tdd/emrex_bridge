import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';

// Split into Server and Client (so we can remove client later)
export type Config = ServerConfig & ClientConfig;

type ServerConfig = {
  SERVER_ACCESS_POINT: string;
  BRIDGE_FRONTEND: string;
};

type ClientConfig = {
  CLIENT_ACCESS_POINT: string;
};

const config = (): Config => ({
  SERVER_ACCESS_POINT: process.env.SERVER_ACCESS_POINT || '',
  CLIENT_ACCESS_POINT: process.env.CLIENT_ACCESS_POINT || '',
  BRIDGE_FRONTEND: process.env.BRIDGE_FRONTEND || '',
});

export default ConfigModule.forRoot({
  envFilePath: '.env',
  load: [config],
  validationSchema: Joi.object({
    SERVER_ACCESS_POINT: Joi.string().required(),
    CLIENT_ACCESS_POINT: Joi.string().required(),
    BRIDGE_FRONTEND: Joi.string().required(),
  }),
});
