<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="public/img/logo-once-only-hub.svg" width="200" alt="Nest Logo" /></a>
    <a href="http://nestjs.com/" target="blank"><img src="public/img/EMREX_logo.png" width="200" alt="Nest Logo" /></a>
</p>

# OOTS - EMREX Bridge

> Backend app for messaging between OOTS and EMREX

## Table of Contents

1. [Description](#description)
2. [Getting started](#getting-started)
3. [Prerequisites](#prerequisites)
4. [Installation](#installation)
4. [Running the app](#running-the-app)
5. [Design](#design)
6. [Licensing](#license)

## Description

This app is an API which allows the connection between `OOTS` & `EMREX` through `Domibus` messages.

## Getting started

In order to run the project it is needed to create a `.env` file locally.

Variables to fill:

- `SERVER_ACCESS_POINT`: Domibus url for server access point
- `CLIENT_ACCESS_POINT`: Domibus url for client access point
- `BRIDGE_FRONTEND`: Url of the emrex_bridge_frontend deployment

## Prerequisites

- [Node.js](https://nodejs.org/en/download/)

## Installation

For the installation of this project it is recommended to have [nvm](https://github.com/nvm-sh/nvm) installed. In case you don't want to install `nvm`, just make sure to use the `NodeJS` version that appears in file `.nvmrc`.

```bash
$ nvm use
$ yarn install
```

## Running the app

### Run the project locally

```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production
$ yarn build
$ yarn start
```

### Run with docker

```bash
$ docker-compose up --build
```

## Design

The design of the project splits into two main parts: client & server. Both of this are APIs that can be called externaly by our two frontends: [OOTS Emrex Procedure Portal](https://code.europa.eu/oots/tdd/emrex_procedure_portal) & [Preview Space](https://code.europa.eu/oots/tdd/emrex_bridge_frontend).

The client is mainly called by the `Bridge App` to send and receive messages to `EMREX` through `Domibus`, send student's data and receive student's information in an academic environment. On the other hand, the server is mainly called by the `Preview Space` to interact directly with `EMREX` with the same purpose.

## License

This software is licensed under [European Union Public License (EUPL) version 1.2.](https://code.europa.eu/oots/tdd/oots_ex/-/blob/main/LICENSE)
